CC = gcc
CFLAGS = -Wall -g

STACK = lib/stack

TEST = lib/test

ITER = lib/iterator


all: stack_test stack_iterator_test

stack_test: bin/stack_test

bin/stack_test: out/stack_basics.o out/stack.o out/stack_test.o
	$(CC) $(CFLAGS) out/stack_basics.o out/stack.o out/stack_test.o -o bin/stack_test

stack_iterator_test: bin/stack_iterator_test

bin/stack_iterator_test: out/iterator.o out/stack_basics.o out/stack.o out/stack_iterator.o out/stack_iterator_test.o
	$(CC) $(CFLAGS) out/iterator.o out/stack_basics.o out/stack.o out/stack_iterator.o out/stack_iterator_test.o -o bin/stack_iterator_test

out/stack_basics.o: $(STACK)/stack_basics.c $(STACK)/stack_basics.h
	$(CC) $(CFLAGS) -c $(STACK)/stack_basics.c -o $@

out/iterator.o: $(ITER)/iterator.c $(ITER)/iterator.h
	$(CC) $(CFLAGS) -c $(ITER)/iterator.c -o $@

out/%test.o : $(TEST)/%test.c
	$(CC) $(CFLAGS) -c $< -o $@

out/%.o : src/%.c 
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f out/*

cleanall: clean
	rm -f bin/*
