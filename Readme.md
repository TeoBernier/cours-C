# cours de C

Plateforme : VS code

Seuls les fichiers dans `/src` sont à modifier.

Toutes les fonctions utilisées sont documentées, il suffit de passer son curseur sur le nom des fonctions pour voir la documentation.

extensions obligatoires :
* C/C++ (Microsoft)
* Doxygen Generator
* Better Comments

il faut fork le projet
Commandes git :
  * git clone \<le projet forked\>
  * git remote add cours https://gitlab.com/TeoBernier/cours-C.git

pour avoir les mises à jour des exercices :
  * git pull cours main