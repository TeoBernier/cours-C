#include "iterator.h"

void* next(t_iterator* iterator) {
    return (iterator->next)(iterator);
}

int empty(t_iterator* iterator) {
    return (iterator->empty)(iterator);
}