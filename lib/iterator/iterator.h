#define in(type, x, iter) ((!empty((t_iterator*)(iter))) && ((x = (type)next((t_iterator*)(iter))) || 1))


typedef struct s_iterator {
    void* (*next)(struct s_iterator*);
    int (*empty)(struct s_iterator*);
} t_iterator;

/**
 * @brief met à jour l'itérateur puis renvoie la valeur itérée
 * 
 * @param iterator l'itérateur
 * @return void* la valeur itérée (peut être un pointeur ou un type plus simple comme un int)
 */
void* next(t_iterator* iterator);

/**
 * @brief check si l'itérateur est vide
 * 
 * @param iterator l'itérateur
 * @return int 0 si vide 1 sinon
 */
int empty(t_iterator* iterator);