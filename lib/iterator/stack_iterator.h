#include <stdio.h>
#include <stdlib.h>
#include "iterator.h"
#include "../utils/macros.h"
#include "../stack/stack.h"

typedef struct s_stack_iterator {
    t_iterator header;
    t_cell* current;
} t_stack_iterator;

/**
 * @brief crée un nouvel itérateur et le renvoie
 * 
 * @param list la liste à itérer
 * @return t_stack_iterator l'itérateur
 */
t_stack_iterator stack_iter(t_list* list);

/**
 * @brief crée un nouvel itérateur mutable et le renvoie
 * 
 * @param list la liste à itérer
 * @return t_stack_iterator l'itérateur
*/
t_stack_iterator stack_iter_mut(t_list* list);

/**
 * @brief crée un nouvel itérateur remplaçant l'ancienne liste et le renvoie
 * 
 * @param list la liste à remplacer
 * @return t_stack_iterator l'itérateur
*/
t_stack_iterator stack_iter_into(t_list* list);