#include <stdio.h>
#include <stdlib.h>
#include "stack_basics.h"


/**
 * @brief ajoute une valeur à la liste
 *
 * @param plist pointeur de liste
 * @param value valeur à ajouter
 *
 * @return t_list* pointeur sur la liste
 */
t_list* push(t_list* plist, int value);

/**
 * @brief enlève un élément de la liste et le renvoie
 *
 * @param plist pointeur de liste
 * @param value valeur à ajouter
 *
 * @return int élément pop
 */
int pop(t_list* plist);





