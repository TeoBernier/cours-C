#include "stack_basics.h"

t_cell* mk_cell(int value, t_cell* next) {
    t_cell* cell = malloc(sizeof(t_cell));

    malloc_check(cell);

    cell->value = value;
    cell->next = next;

    return cell;
}

int del_cell(t_cell* cell) {
    int value = cell->value;

    // clean memory
    cell->value = 0;
    cell->next = NULL;

    // free memory
    free(cell);
    
    return value;
}