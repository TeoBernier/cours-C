#include <stdio.h>
#include <stdlib.h>
#include "../utils/macros.h"

/**
 * @brief cellule chaînée contenant un entier
 * 
 */
typedef struct s_cell {
    int value;
    struct s_cell* next;
} t_cell;

// 
// 

/**
 * @brief liste d'entier
 * 
 * identique à t_cell*
 * mais plus clair car on a un pointeur head explicite
 */
typedef struct s_list {
    t_cell* head;
} t_list;



/**
 * @brief alloue une nouvelle cell, l'initialise et renvoie son adresse
 *
 * @param value valeur de la nouvelle cell
 * @param next cell suivant
 *
 * @return t_cell* pointeur sur la cell
 */
t_cell* mk_cell(int value, t_cell* next);

/**
 * @brief clean les fields d'une cell, la free et renvoie la valeur qu'elle contenait
 *
 * @param cell pointeur de cell
 *
 * @return int valeur de la cell
 */
int del_cell(t_cell* cell);