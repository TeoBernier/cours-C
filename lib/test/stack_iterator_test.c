#include "../iterator/stack_iterator.h"


int main(void) {
    t_list list = {NULL};

    push(push(push(push(push(push(push(&list, 1), 2), 3), 4), 5), 6), 7);

    t_stack_iterator iter;
    int x;

    iter = stack_iter(&list);

    printf("[");
    if in(int, x, &iter) printf("%d", x);
    while in(int, x, &iter) {
        printf(", %d", x);
    }
    printf("]\n");

    int *p;
    iter = stack_iter_mut(&list);
    while in(int*, p, &iter) {
        *p *= 2;
    }


    iter = stack_iter(&list);
    printf("[");
    if in(int, x, &iter) printf("%d", x);
    while in(int, x, &iter) {
        printf(", %d", x);
    }
    printf("]\n");

    
    iter = stack_iter_into(&list);
    printf("[");
    if in(int, x, &iter) printf("%d", x);
    while in(int, x, &iter) {
        printf(", %d", x);
    }
    printf("]\n");

    dbg_ptr(list.head, NULL);


    return EXIT_SUCCESS;
}