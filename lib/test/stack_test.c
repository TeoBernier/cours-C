#include "../stack/stack.h"


int main(void) {
    t_list list = {NULL};
    dbg_ptr(list.head, NULL);

    push(&list, 1);
    dbg_ptr_ne(list.head, NULL);
    dbg_int(list.head->value, 1);

    push(&list, 3);
    dbg_ptr_ne(list.head, NULL);
    dbg_int(list.head->value, 3);

    push(&list, 6);
    dbg_ptr_ne(list.head, NULL);
    dbg_int(list.head->value, 6);

    dbg_int(pop(&list), 6);
    dbg_ptr_ne(list.head, NULL);

    dbg_int(pop(&list), 3);
    dbg_ptr_ne(list.head, NULL);

    push(&list, 10);
    dbg_ptr_ne(list.head, NULL);
    dbg_int(list.head->value, 10);

    dbg_int(pop(&list), 10);
    dbg_ptr_ne(list.head, NULL);

    dbg_int(pop(&list), 1);
    dbg_ptr(list.head, NULL);

    return EXIT_SUCCESS;
}