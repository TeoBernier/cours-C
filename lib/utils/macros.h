#define dbg_int(x, y) {int _x = x, _y = y; \
                          if (_x == _y) {printf("Assert '" #x " == " #y "'\tsucceed ... V\n");} \
                          else   {       printf("Assert '" #x " == " #y "'\tfailed .... X -> %d != %d\n", _x, _y);}}

#define dbg_int_ne(x, y) {int _x = x, _y = y; \
                          if (_x != _y) {printf("Assert '" #x " != " #y "'\tsucceed ... V\n");} \
                          else   {       printf("Assert '" #x " != " #y "'\tfailed .... X -> %d == %d\n", _x, _y);}}

#define dbg_ptr(x, y) {void* _x  = (void*)(x),* _y = (void*)(y); \
                          if (_x == _y) {printf("Assert '" #x " == " #y "'\tsucceed ... V\n");} \
                          else   {       printf("Assert '" #x " == " #y "'\tfailed .... X -> %p != %p\n", _x, _y);}}

#define dbg_ptr_ne(x, y) {void* _x  = (void*)(x),* _y = (void*)(y); \
                          if (_x != _y) {printf("Assert '" #x " != " #y "'\tsucceed ... V\n");} \
                          else   {       printf("Assert '" #x " != " #y "'\tfailed .... X -> %p == %p\n", _x, _y);}}
                          
#define malloc_check(addr) {if (addr == NULL) { \
                                fprintf(stderr, "Error : Out of memory...\n"); \
                                exit(EXIT_FAILURE); \
                            }}
