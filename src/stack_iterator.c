#include "../lib/iterator/stack_iterator.h"

/**
 * TODO écrire la condition et la renvoyer
 * 
 * @brief renvoie true si la liste est vide et false sinon
 * 
 * @param iterator l'itérateur
 * @return int 1 si vide, 0 sinon
 */
int stack_empty(t_iterator* iterator){
    t_stack_iterator* iter = (t_stack_iterator*)iterator;
    // ...
}

/**
 * TODO écrire l'itération sans oublier de renvoyer la valeur de l'ancien élément
 * 
 * @brief renvoie la valeur itérée, tout en faisant avancer l'itérateur
 * 
 * @param iterator l'itérateur
 * @return int la valeur actuellement itérée (le void* vient du fait que l'on fait qql chose de générique)
 */
void* stack_next(t_iterator* iterator) {
    t_stack_iterator* iter = (t_stack_iterator*)iterator;
    // ...
}


t_stack_iterator stack_iter(t_list* list) {
    t_iterator header = {stack_next, stack_empty};
    t_stack_iterator iter = {header, list->head};
    return iter;
}


/**
 * TODO écrire l'itération sans oublier de renvoyer la valeur de l'ancien élément
 * 
 * @brief renvoie la valeur itérée, tout en faisant avancer l'itérateur
 * 
 * @param iterator l'itérateur
 * @return int* pointeur vers la valeur actuellement itérée
 */
void* stack_next_mut(t_iterator* iterator) {
    t_stack_iterator* iter = (t_stack_iterator*)iterator;
    // ...
}


/**
 * TODO tout comme le premier exemple, il faut créer l'itérateur et le renvoyer
 */
t_stack_iterator stack_iter_mut(t_list* list) {
    // ...
}

// * le suffixe into viens du fait que l'on prend possession de la liste et que l'ancienne liste devient inutilisable.

/**
 * TODO écrire l'itération sans oublier de renvoyer la valeur de l'ancien élément (ici, on doit free les éléments au fur et à mesure)
 * 
 * @brief renvoie la valeur itérée, tout en faisant avancer l'itérateur
 * 
 * @param iterator l'itérateur
 * @return int la valeur actuellement itérée
 */
void* stack_next_into(t_iterator* iterator) {
    t_stack_iterator* iter = (t_stack_iterator*)iterator;
    // ...
}



/**
 * TODO tout comme le premier exemple, il faut créer l'itérateur et le renvoyer
 */
t_stack_iterator stack_iter_into(t_list* list) {
    // ...
    list->head = NULL;
    // ...
}